package com.gregskl.mousecontrolclient;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by magshimim on 1/4/2017.
 */

public class TcpConnector {
    //TODO add tcp connection stuff which include: double click, and scouting the network for a responding server
    Socket socket;
    PrintWriter out;

    public TcpConnector(int port, String host, Context context) {
        try {
            socket = new Socket(host, port);
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, "Couldn't create TCP socket", Toast.LENGTH_LONG).show();
        }
    }

    public void sendMessage(String message) {
        out.println(message);
        out.flush();
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
