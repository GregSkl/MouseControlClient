package com.gregskl.mousecontrolclient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private DrawingView dv;
    private Paint mPaint;
    private UdpConnector udp;
    private TcpConnector tcp;
    private Vibrator vibrator;
    private int x = -1; //Initialized to -1 to prevent a move event registering before touch event
    private int y = -1;

    //Add refresh connection button
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dv = new DrawingView(this);
        udp = new UdpConnector(6117, "192.168.1.12", this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                tcp = new TcpConnector(6118, "192.168.1.12", MainActivity.this);
            }
        }).start();
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        setContentView(dv);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.GREEN);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(0);
    }

    public class DrawingView extends View {

        private GestureDetector gestureDetector;
        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        Context context;
        private Paint circlePaint;
        private Path circlePath;

        public DrawingView(Context c) {
            super(c);
            context=c;
            gestureDetector = new GestureDetector(context, new GestureListener());
            mPath = new Path();
            circlePaint = new Paint();
            circlePath = new Path();
            circlePaint.setAntiAlias(true);
            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setStrokeJoin(Paint.Join.MITER);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawPath( circlePath,  circlePaint);
        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 1;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
                mX = x;
                mY = y;
                circlePath.reset();
                circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
            }
        }

        private void touch_up() {
            mPath.lineTo(mX, mY);
            circlePath.reset();
            mCanvas.drawPath(mPath,  mPaint);
            mPath.reset();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float newX = event.getX();
            float newY = event.getY();
            //Log.d("Point", "X:" + newX + " Y:" + newY);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(newX, newY);
                    x = (int) newX;
                    y = (int) newY;
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(newX, newY);
                    invalidate();

                    //TODO get a correct difference and send a binary to the server
                    int x_diff = (int) newX - x;
                    int y_diff = (int) newY - y;
                    if(x_diff != 0 || y_diff != 0)
                        udp.sendMessage(x_diff, y_diff);
                    x = (int) newX;
                    y = (int) newY;
                    Log.d("Diff", "X: " + x_diff + " Y: " + y_diff);
                    break;
                case MotionEvent.ACTION_UP:
                    x = -1;
                    y = -1;
                    touch_up();
                    invalidate();
                    break;
            }
            return gestureDetector.onTouchEvent(event);
        }
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            tcp.sendMessage("L");
            return true;
        }

        @Override
        public void onLongPress(MotionEvent event) {
            tcp.sendMessage("R");
            vibrator.vibrate(25);
            super.onLongPress(event);
        }
    }
}
