package com.gregskl.mousecontrolclient;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by magshimim on 1/4/2017.
 */

public class UdpConnector {

    int port;
    DatagramSocket socket;
    InetAddress local;
    Context context;

    public UdpConnector(int port, String host, Context context) {
        this.port = port;
        this.context = context;
        try {
            socket = new DatagramSocket();
            local = InetAddress.getByName(host);
        } catch (SocketException e) {
            e.printStackTrace();
            Toast.makeText(context, "Couldn't create UDP socket", Toast.LENGTH_LONG).show();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            Toast.makeText(context, "Couldn't connect to host via UDP", Toast.LENGTH_LONG).show();
        }
    }

    public void sendMessage(int x, int y) {
        byte[] message = new byte[2];
        message[0] = (byte) x;
        message[1] = (byte) y;
        final DatagramPacket packet = new DatagramPacket(message, 2, local, port);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Couldn't send UDP packet", Toast.LENGTH_LONG).show();
                }
            }
        }).start();
    }
}
